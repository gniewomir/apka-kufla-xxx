package com.example.matrial.my2ndapp;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matrial on 2016-05-09.
 */
public class ChosenCategoryHolder {
    private static List<String> listOfChosenCategory = new ArrayList<>();
    public ChosenCategoryHolder() {
    }
    public static void logAll(){
        for (String a : listOfChosenCategory){
            Log.i(MainActivity.TAG, "Oto element naszej magicznej statycznej listy:..." + a);
        }
    }
    public static void addElement(String a){
        if(!listOfChosenCategory.contains(a)) {
            listOfChosenCategory.add(a);
        }
    }
    public static List<String> getListOfChosenCategory() {
        return listOfChosenCategory;
    }
    public static void cleanList (){
        listOfChosenCategory.clear();
    }
}
