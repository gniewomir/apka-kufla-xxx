package com.example.matrial.my2ndapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class MainQuotationActivity extends AppCompatActivity {
    DBHelper dbFavourites;
    Integer actualQuotationPointer;
    Integer amoutOfQuotations;
    Integer [] favouritesIdsTable;
    String [][] quotesAuthors;
    TextSwitcher ts1;
    TextSwitcher ts2;
    Switch switcher;
    boolean flag = true;
    List<Integer> temporaryBookmarked;
    SeekBar seekBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_quotation);

        Intent intent = getIntent();
        Integer choice = intent.getIntExtra("Choice", -1);
        Integer firstQuotateion = intent.getIntExtra("quoteId", -1);
        dbFavourites = new DBHelper(this);

        //Inicjalizacja Zmiennych
        temporaryBookmarked = new ArrayList<Integer>();
        favouritesIdsTable = dbFavourites.getFavouritesIds();
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        quotesAuthors = getQuotes(choice);
        actualQuotationPointer = setStartingQuotationPointer(firstQuotateion, quotesAuthors);
        switcher = (Switch)findViewById(R.id.switchFavourites);
        ts1 = (TextSwitcher) findViewById(R.id.textSwitcherMainQuotation);
        AdView mAdView = (AdView) findViewById(R.id.adView);
        Button buttonShatre = (Button) findViewById(R.id.shareButton);

        // ADMOB OPERATIONS
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("9519800193930C02E4EC50EA31C97281")
                .build();
        mAdView.loadAd(adRequest);

        // SHARING TOOL

        buttonShatre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareIt();
            }
        });


        // DLA BRAKU ULUBIONYCH
        if ( quotesAuthors[0][0] == "Sorry, but you haven't added any favourites yet") {

            ts1.setText("Sorry, but you haven't added any favourites yet");
            switcher.setVisibility(View.GONE);
        } else { // DLA ULUBIONYCH

            amoutOfQuotations = quotesAuthors[0].length;
            seekBar.setMax(amoutOfQuotations - 1);
            seekBar.setProgress(actualQuotationPointer);
            ts2 = (TextSwitcher) findViewById(R.id.textSwitcherAuthors);
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.relLayout);
            switcher = (Switch) findViewById(R.id.switchFavourites);

            // SWITCHER
            switcher.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!switcher.isChecked()){
                        switcher.setChecked(false);
                        if(temporaryBookmarked.contains(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))){
                            temporaryBookmarked.remove(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]));
                        }
                        for ( Integer i = 0; i < favouritesIdsTable.length ; i++){
                            if (Integer.valueOf(quotesAuthors[2][actualQuotationPointer]) == favouritesIdsTable[i]){
                                favouritesIdsTable[i] = -1;
                            }
                        }
                        dbFavourites.deleteFromBookmarks(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]));
                    } else {
                        switcher.setChecked(true);
                        dbFavourites.addToBookmarks(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]));
                        temporaryBookmarked.add(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]));
                    }
                }
            });
            // SWITCHER END

            setStartingQuote();


            // SETTING ONTOUCHES AND TEXTSWICHER TEXT
            animLeftSwitcher();
            assert rl != null;
            rl.setOnTouchListener(new OnSwipeTouchListener(MainQuotationActivity.this) {
                public void onSwipeRight() {
                    if (!flag) {
                        animRightSwitcher();
                    }
                    if (actualQuotationPointer == amoutOfQuotations - 1) {
                        actualQuotationPointer -= amoutOfQuotations;
                    }
                    actualQuotationPointer += 1;
                    ts1.setText(quotesAuthors[1][actualQuotationPointer]);
                    ts2.setText(quotesAuthors[0][actualQuotationPointer]);
                    seekBar.setProgress(actualQuotationPointer);
                    if (checkIfFavouriteMatche(favouritesIdsTable, Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))
                            || temporaryBookmarked.contains(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))) {
                        switcher.setChecked(true);
                    } else {
                        switcher.setChecked(false);
                    }

                    flag = true;
                }

                public void onSwipeLeft() {
                    if (flag) {
                        animLeftSwitcher();
                    }
                    if (actualQuotationPointer == 0) {
                        actualQuotationPointer = amoutOfQuotations;
                    }
                    actualQuotationPointer -= 1;
                    seekBar.setProgress(actualQuotationPointer);
                    ts1.setText(quotesAuthors[1][actualQuotationPointer]);
                    ts2.setText(quotesAuthors[0][actualQuotationPointer]);
                    if (checkIfFavouriteMatche(favouritesIdsTable, Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))
                            || temporaryBookmarked.contains(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))) {
                        switcher.setChecked(true);
                    } else {
                        switcher.setChecked(false);
                    }
                    flag = false;
                }
            });

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    actualQuotationPointer = i;
                    ts1.setText(quotesAuthors[1][actualQuotationPointer]);
                    ts2.setText(quotesAuthors[0][actualQuotationPointer]);

                    if (checkIfFavouriteMatche(favouritesIdsTable, Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))
                            || temporaryBookmarked.contains(Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))) {
                        switcher.setChecked(true);
                    } else {
                        switcher.setChecked(false);
                    }
                    flag = true;
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });

            ts2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Log.i(MainActivity.TAG, "ts2.onclick Sprawdzam, czy getQuotes dobrze odczytal dlugość przed zmiana " + amoutOfQuotations);
                    quotesAuthors = null;
                    quotesAuthors = getQuotes(2);
                    amoutOfQuotations = quotesAuthors[0].length;
                    actualQuotationPointer = 0;
                    seekBar.setProgress(actualQuotationPointer);
                    seekBar.setMax(amoutOfQuotations - 1);

                    setStartingQuote();
                }
            });


        } // END OF ELSE ( MORE THEN ZERO QUOTES
    } // ENDO OF ONCREATE
    private void setStartingQuote(){
        ts1.setText(quotesAuthors[1][actualQuotationPointer]);
        ts2.setText(quotesAuthors[0][actualQuotationPointer]);
        if (checkIfFavouriteMatche(favouritesIdsTable, Integer.valueOf(quotesAuthors[2][actualQuotationPointer]))) {
            switcher.setChecked(true);
        }
}
    private void shareIt() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, quotesAuthors[1][actualQuotationPointer] + "\n"
                + quotesAuthors[0][actualQuotationPointer] );
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    private boolean checkIfFavouriteMatche (Integer [] favoTable, Integer id){
        boolean flaga = false;
        for (Integer favoId : favoTable){
            if (favoId == id){
                flaga = true;
                break;
            }
        }
        return flaga;
    }




    private String[][] getQuotes(Integer choice) {
        String [][] quotes;
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();

        if ( choice == 1 ){
            quotes = databaseAccess.getQuotes(favouritesIdsTable);
        } else if (choice == 2) {
            TextView currentlyShownText = (TextView) ts2.getCurrentView();
            quotes = databaseAccess.getQuotesByAuthor(currentlyShownText.getText().toString());
        } else if (choice == 3) {
            quotes = databaseAccess.getQuotes();
            ChosenCategoryHolder.cleanList();
        } else {
            quotes = databaseAccess.getQuotes("*");
        }

        databaseAccess.close();
        return quotes;
    }

    private Integer setStartingQuotationPointer(Integer firstQuotationId, String[][] quotesAuthors){
        if (firstQuotationId == -1) {
            return 0;
        } else {
            Integer counter = 0;
            for (String id : quotesAuthors[2]){
                if (id == String.valueOf(firstQuotationId)) {
                    break;
                } else {
                    counter++;
                }
            }
            return counter;
        }
    }


    private void animLeftSwitcher(){
        ts1.setInAnimation(this, R.anim.slide_in_right);
        ts1.setOutAnimation(this, R.anim.slide_out_left);
        ts2.setInAnimation(this, R.anim.slide_in_right);
        ts2.setOutAnimation(this, R.anim.slide_out_left);
    }
    private void animRightSwitcher(){
        ts1.setInAnimation(this, R.anim.slide_in_left);
        ts1.setOutAnimation(this, R.anim.slide_out_right);
        ts2.setInAnimation(this, R.anim.slide_in_left);
        ts2.setOutAnimation(this, R.anim.slide_out_right);
    }

}
