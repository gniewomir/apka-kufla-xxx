package com.example.matrial.my2ndapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.util.Log;

/**
 * Created by Matrial on 2016-05-01.
 */


public class DBHelper extends SQLiteOpenHelper {
    private SQLiteDatabase database;
    public static final String DB_NAME = "DB_FAVOURITES";
    public static final String TABLE_FAVOURITES_NAME = "TABLE_FAVOURITES";
    public static final String COL_ID = "ID";
    public static final String COL_QUOTE_ID = "DB_FAVOURITES";
    public static final String CREATE_TABLE_FAVOURITES = "CREATE TABLE " + TABLE_FAVOURITES_NAME +
            " (" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COL_QUOTE_ID + " INTEGER)";
    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_FAVOURITES_NAME;
    public static final Integer DATABASE_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FAVOURITES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void open(){
        database = this.getReadableDatabase();
    }
    public void close(){
        if (database != null){
            this.database.close();
        }
    }

    public long addToBookmarks(Integer id){
        open();
        ContentValues cv = new ContentValues();
        cv.put(COL_QUOTE_ID, id);
        long a = database.insert(TABLE_FAVOURITES_NAME, null, cv);
        Log.i("*** NUMER DODANEGO REKORDU", String.valueOf(a));
        close();
        return  a;
    }
    public void deleteFromBookmarks(Integer id){
        open();
        Integer a = database.delete(TABLE_FAVOURITES_NAME, COL_QUOTE_ID + " = " + id, null);
        Log.i("*** NUMER DODANEGO REKORDU", String.valueOf(a));
        close();
    }
    public Integer [] getFavouritesIds() {
        Log.i(MainActivity.TAG, "Wlaczam funkcje getFavouritesIds");
        open();
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_FAVOURITES_NAME, null);
        Integer size = cursor.getCount();
        Integer [] favoTable = new Integer[size];
        cursor.moveToFirst();
        Integer iterator = 0;
        Log.i(MainActivity.TAG, "WLACZAM PETLE! " + size );
        while (!cursor.isAfterLast()){
            favoTable[iterator] = cursor.getInt(1);
            Log.i(MainActivity.TAG, String.valueOf(cursor.getInt(1)));
            iterator++;
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return favoTable;
    }
}
