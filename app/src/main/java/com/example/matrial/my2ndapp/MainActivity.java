package com.example.matrial.my2ndapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity {
    public static final String TAG = "DEBUGGER";
    private PendingIntent pIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 4);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Intent myIntent = new Intent(MainActivity.this, MyPhoneReciver.class);
        pIntent = PendingIntent.getBroadcast(MainActivity.this, 0 , myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                24*60*60*1000, pIntent);

        Button bRandom = (Button)findViewById(R.id.bRandom);
        Button bFavourites = (Button)findViewById(R.id.bFavourites);
        Button bCategories = (Button)findViewById(R.id.bCategories);
        assert bRandom != null;
        bRandom.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainQuotationActivity.class);
                intent.putExtra("Choice", -1);
                startActivity(intent);
            }
        });

        assert bFavourites != null;
        bFavourites.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainQuotationActivity.class);
                intent.putExtra("Choice", 1);
                startActivity(intent);
            }
        });

        assert bCategories != null;
        bCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CategoriesActivity.class);
                startActivity(intent);
            }
        });
    }

}