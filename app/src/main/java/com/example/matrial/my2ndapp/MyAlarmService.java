package com.example.matrial.my2ndapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by Matrial on 2016-05-04.
 */
public class MyAlarmService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @SuppressWarnings("static-access")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.i("asd", "ASD");
        DatabaseAccess dbAccess = DatabaseAccess.getInstance(this);
        dbAccess.open();
        String [] quoteOfTheDay = dbAccess.getQuoteOfTheDay();
        dbAccess.close();

        /* 0 - Author
         * 1 - Quotation
         * 2 - ID
         * */

        Intent resultIntent = new Intent(this.getApplicationContext(), MainQuotationActivity.class);
        resultIntent.putExtra("Choice", -1);
        resultIntent.putExtra("quoteId", Integer.valueOf(quoteOfTheDay[2]));
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this.getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this.getApplicationContext())
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentTitle("Quote of the day")
                        .setContentText(quoteOfTheDay[1] + "\n" + quoteOfTheDay[0])
                        .setContentIntent(resultPendingIntent);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotifyMgr =
                (NotificationManager) this.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.notify(1, mBuilder.build());


        return flags;
    }
}
