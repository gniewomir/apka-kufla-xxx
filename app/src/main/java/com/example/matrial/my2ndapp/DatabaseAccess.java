package com.example.matrial.my2ndapp;

/**
 * Created by Matrial on 2016-04-29.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to avoid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */
    public String[][] getQuotes(String choice) {
        Cursor cursor = database.rawQuery("SELECT " + choice + " FROM quotations", null);
        Integer size = cursor.getCount();
        String [][] table = new String[4][size];
        Integer counter = 0;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            table[0][counter] = cursor.getString(1);
            table[1][counter] = cursor.getString(3);
            table[2][counter] = String.valueOf(cursor.getInt(0));
            counter++;
            cursor.moveToNext();
        }
        cursor.close();
        return table;
    }

    public String[][] getQuotes(Integer[] favouritesIdsTable) {
        /**
         * 0 - Author
         * 1 - Quotation
         * 2 - ID
         */
        Cursor cursor = null;
        Integer size;
        Integer favoAmout;
        try {
             favoAmout = favouritesIdsTable.length;
        } catch (NullPointerException npe) {
            favoAmout = 0;
        }
        Log.i(MainActivity.TAG, "Ile mamy ulubionyc" + favoAmout);
        // BRAK ULUBIONYCH
        if ( favoAmout == 0) {
            String [][] table = new String[1][3];
            table[0][0] = "Sorry, but you haven't added any favourites yet";
            table[0][1] = "";
            table[0][2] = "";

            return table;
        }
        // X ULUBIONYCH
        String [][] table = new String [3][favoAmout];

        Integer counter = 0;
        for (Integer favoId : favouritesIdsTable){
            cursor = database.rawQuery("SELECT * FROM quotations WHERE _id =" + favoId, null);
            size = cursor.getCount();
            cursor.moveToFirst();
            if (size == 1) {
                table[0][counter] = cursor.getString(1);
                table[1][counter] = cursor.getString(3);
                table[2][counter] = String.valueOf(cursor.getInt(0));
                counter++;
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return table;
    }
    public String [] getQuoteOfTheDay (){
        Cursor cursor = database.rawQuery("SELECT * FROM quotations", null);
        Integer size = cursor.getCount();
        String [] table = new String[3];
        Random rand = new Random();
        Integer randNum = rand.nextInt(size);
        cursor.moveToFirst();
        for (Integer i = 0; i < randNum ; i++){
            cursor.moveToNext();
        }
        table[0] = cursor.getString(1);
        table[1] = cursor.getString(3);
        table[2] = String.valueOf(cursor.getInt(0));
        cursor.close();
        return table;
    }

    public String[][] getQuotesByAuthor(String category) {
        Cursor cursor = database.rawQuery("SELECT * FROM quotations WHERE author = '" + category + "'", null);
        Integer size = cursor.getCount();
        String [][] table = new String[4][size];
        Integer counter = 0;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            table[0][counter] = cursor.getString(1);
            table[1][counter] = cursor.getString(3);
            table[2][counter] = String.valueOf(cursor.getInt(0));
            counter++;
            cursor.moveToNext();
        }
        cursor.close();
        return table;
    }

    public String[] getCategories() {
        Cursor cursor = database.rawQuery("SELECT DISTINCT category FROM quotations", null);
        Integer size = cursor.getCount();
        String [] table = new String[size];
        cursor.moveToFirst();
        Integer counter = 0;
        while (!cursor.isAfterLast()){
            table[counter] = cursor.getString(0);
            counter += 1;
            cursor.moveToNext();
        }
        cursor.close();
        return table;
    }


    public String[][] getQuotes() {
        List<String> categoriesList = ChosenCategoryHolder.getListOfChosenCategory();
        Integer  numberOfCategories = categoriesList.size();
        Cursor [] cursors = new Cursor[numberOfCategories];
        Integer counter = 0;
        Integer totalSizeOfCursors = 0;
        for (String category : categoriesList) {
            cursors [counter] = database.rawQuery("SELECT * FROM quotations WHERE category LIKE '%" + category + "%'", null);
            totalSizeOfCursors += cursors[counter].getCount();
            counter ++;
        }
        counter = 0;
        List<Integer> idsTable = new ArrayList<Integer>();
        String [][] table = new String[3][totalSizeOfCursors];
        for ( Integer a = 0; a < numberOfCategories ; a++) {
            Log.i(MainActivity.TAG, "Odpalilem fora");
            cursors[a].moveToFirst();
            table[0][counter] = cursors[a].getString(1);
            table[1][counter] = cursors[a].getString(3);
            table[2][counter] = String.valueOf(cursors[a].getInt(0));
            counter++;
            while (cursors[a].moveToNext()){
                Log.i(MainActivity.TAG, "counter przed ustawianiem" + counter);
                table[0][counter] = cursors[a].getString(1);
                table[1][counter] = cursors[a].getString(3);
                table[2][counter] = String.valueOf(cursors[a].getInt(0));
                counter++;
            }
        }
        for (Cursor a : cursors){
            a.close();
        }
        String [][] cleanTable = removeDuplitacesFromTable(table);
        return cleanTable;
    }
    private String [][] removeDuplitacesFromTable(String [][] table){
        Integer prevSize = table[2].length;
        for (String a : table[2]){

        }


        return table;
    }

}