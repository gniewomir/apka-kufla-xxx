package com.example.matrial.my2ndapp;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;


public class CategoryAdapter extends ArrayAdapter<Category> {

    private List<Category> list;
    private final Activity context;
    boolean checkAll_dlag = false;
    boolean checkItem_flag = false;

    public CategoryAdapter(Activity context, List<Category> list) {
        super(context, R.layout.listviewitem, list);
        this.list = list;
        this.context = context;
    }
    private static class ViewHolder {
        public TextView text;
        public CheckBox checkbox;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if(convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.listviewitem, null);
            viewHolder = new ViewHolder();
            viewHolder.text = (TextView)convertView.findViewById(R.id.categoryTextView);
            viewHolder.checkbox = (CheckBox)convertView.findViewById(R.id.checkBoxCategory);

            final ViewHolder finalViewHolder = viewHolder;
            final ViewHolder finalViewHolder1 = viewHolder;
            viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int getPosition = (Integer)buttonView.getTag();
                    list.get(getPosition).setSelected(buttonView.isChecked());
                    ChosenCategoryHolder.addElement((String) finalViewHolder1.text.getText());

                }
            });
            convertView.setTag(viewHolder);
            convertView.setTag(R.id.categoryTextView, viewHolder.text);
            convertView.setTag(R.id.checkBoxCategory, viewHolder.checkbox);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.checkbox.setTag(position);
        viewHolder.text.setText(list.get(position).getName());
        viewHolder.checkbox.setChecked(list.get(position).isSelected());

        return convertView;
    }

}
