package com.example.matrial.my2ndapp;

/**
 * Created by Matrial on 2016-05-07.
 */

public class Category {
        String name;
        boolean selected = false;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
