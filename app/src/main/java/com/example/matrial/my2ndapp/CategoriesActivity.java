package com.example.matrial.my2ndapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView lv;
    ArrayAdapter<Category> adapter;
    List<Category> list = new ArrayList<Category>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        lv = (ListView)findViewById(R.id.listView);
        adapter = new CategoryAdapter(this, getCategory());
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        Button button = (Button)findViewById(R.id.buttonstartcategories);

        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ChosenCategoryHolder.getListOfChosenCategory().isEmpty()){
                    Toast.makeText(CategoriesActivity.this, "You haven't chosen any categories yet!", Toast.LENGTH_SHORT).show();
                } else {
                Intent intent = new Intent(CategoriesActivity.this, MainQuotationActivity.class);
                intent.putExtra("Choice", 3);
                startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View v, int i, long l) {
        TextView label = (TextView) v.getTag(R.id.categoryTextView);
        CheckBox checkbox = (CheckBox) v.getTag(R.id.checkBoxCategory);
        if (!isCheckedOrNot(checkbox)){
            checkbox.setChecked(true);
            } else {
            checkbox.setChecked(false);
        }
        ChosenCategoryHolder.addElement((String) label.getText());
        ChosenCategoryHolder.logAll();
    }
    private boolean isCheckedOrNot(CheckBox checkbox) {
        if(checkbox.isChecked())
            return true;
        else
            return false;
    }


    private String [] getCategories() {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        String [] categories = databaseAccess.getCategories();
        databaseAccess.close();
        return categories;
    }

    public List<Category> getCategory() {
        String[] messyCategories = getCategories();
        List<String> lista = new ArrayList<String>();
        List<Category> categoryList = new ArrayList<>();

        Integer charLenghtCounter;
        Integer pointer;
        for (String word : messyCategories) {
            pointer = 0;
            charLenghtCounter = word.length();

            for (Integer i = 0; i < charLenghtCounter; i++) {
                if (word.charAt(i) == '-') {
                    if (i > 0) {
                        if (!lista.contains(word.substring(pointer, i))) {
                            lista.add(word.substring(pointer, i));
                        }
                    }
                    pointer = i + 1;
                }
            }
        }
        for (String a : lista){
            categoryList.add(new Category(a));
        }

        return categoryList;
    }
}
